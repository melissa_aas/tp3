package seleniumTests;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class testLoginTeacher extends BaseForTests {

    @Test
    void loginTeacher()  {
        login("Turing", "turing");
        assertTrue(driver.getTitle().contains("Page des TER de M1"));
    }

    @Test
    void TeacherAddTeacher() {
        loginTeacher();
        click(driver.findElement(By.xpath("//a[@href='/listTeachers']")));
        click(driver.findElement(By.xpath("//a[@href='addTeacher']")));

        assertTrue(driver.getTitle().contains("Error"));
    }

    @Test
    void ManagerAddTeacher() throws IOException {
        login("Chef", "mdp");
        click(driver.findElement(By.xpath("//a[@href='/listTeachers']")));
        click(driver.findElement(By.xpath("//a[@href='addTeacher']")));

        WebElement inputTeacherFirstName = driver.findElement(By.id("firstName"));
        WebElement inputTeacherLastName = driver.findElement(By.id("lastName"));
        WebElement submit = driver.findElement(By.cssSelector("[type=submit]"));

        write(inputTeacherFirstName, "TeacherfirstnameforTest");
        write(inputTeacherLastName, "TeacherLastNameForTest");
        click(submit);
        screenshot("screenshot");

        assertTrue(driver.getPageSource().contains("TeacherLastNameForTest"));
    }
}
