package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import um.fds.agl.ter22.entities.Assignment;
import um.fds.agl.ter22.forms.AssignmentForm;
import um.fds.agl.ter22.services.AssignmentService;
import um.fds.agl.ter22.services.TeacherService;

@Controller
public class AssignmentController implements ErrorController {

    @Autowired
    private TeacherService teacherService;
    @Autowired
    private AssignmentService assignmentService;

    @GetMapping("/listAssignments")
    public Iterable<Assignment> getAssignments(Model model) {
        model.addAttribute("assignments", assignmentService.getAssignments());
        return assignmentService.getAssignments();
    }
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @GetMapping(value = { "/addAssignment" })
    public String showAddPersonPage(Model model) {

        AssignmentForm assignmentForm = new AssignmentForm();
        model.addAttribute("assignmentForm", assignmentForm);

        return "addAssignment";
    }

    @PostMapping(value = { "/addAssignment"})
    public String addAssignment(Model model, @ModelAttribute("AssignmentForm") AssignmentForm assignmentForm) {
        Assignment assignment;
        if(assignmentService.findById(assignmentForm.getId()).isPresent()){
            assignment = assignmentService.findById(assignmentForm.getId()).get();
            assignment.setTitle(assignmentForm.getTitle());
            assignment.setSupervisor(assignmentForm.getSupervisor());
        } else {
            assignment = new Assignment(assignmentForm.getTitle(), assignmentForm.getSupervisor());
        }
        assignmentService.saveAssignment(assignment);
        return "redirect:/listAssignments";

    }

    @GetMapping(value = {"/showAssignmentUpdateForm/{id}"})
    public String showAssignmentUpdateForm(Model model, @PathVariable(value = "id") long id){

        AssignmentForm assignmentForm = new AssignmentForm(id, assignmentService.findById(id).get().getTitle(), assignmentService.findById(id).get().getSupervisor());
        model.addAttribute("assignmentForm", assignmentForm);
        return "updateAssignment";
    }

    @GetMapping(value = {"/deleteAssignment/{id}"})
    public String deleteAssignment(Model model, @PathVariable(value = "id") long id){
        assignmentService.deleteAssignment(id);
        return "redirect:/listAssignments";
    }
}
