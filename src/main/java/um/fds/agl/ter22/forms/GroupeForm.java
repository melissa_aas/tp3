package um.fds.agl.ter22.forms;

public class GroupeForm {
    private long id;
    private String title;

    public GroupeForm(long id, String title) {
        this.title = title;
        this.id = id;
    }

    public GroupeForm() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}


