package um.fds.agl.ter22.forms;

import um.fds.agl.ter22.entities.Teacher;

import java.util.ArrayList;
import java.util.List;

public class AssignmentForm {
    private long id;
    private String title;
    private String supervisor;
    private List<Teacher> secondaryInCharge = new ArrayList<Teacher>();

    public AssignmentForm(long id, String title, String supervisor) {
        this.title = title;
        this.id = id;
        this.supervisor = supervisor;
        this.secondaryInCharge = secondaryInCharge;
    }

    public AssignmentForm() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getSupervisor() {
        return supervisor;
    }
    public List<Teacher> getSecondaryInCharge(){
        return secondaryInCharge;
    }
}
