package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.Groupe;

public interface GroupeRepository<T extends Groupe> extends CrudRepository<T, Long> {

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    Groupe save(@Param("groupe") Groupe groupe);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void deleteById(@Param("id") Long id);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void delete(@Param("groupe") Groupe groupe);
}
