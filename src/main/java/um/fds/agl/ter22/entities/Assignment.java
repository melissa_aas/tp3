package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.ArrayList;


@Entity
public class Assignment{
    private @Id @GeneratedValue long id;
    private String title;
    private String supervisor;


    public Assignment(long id, String title, String supervisor){
        this.id = id;
        this.title = title;
        this.supervisor = supervisor;
    }

    public Assignment(String title, String supervisor){
        this.title = title;
        this.supervisor = supervisor;
    }

    public Assignment() {}

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getSupervisor() {
        return supervisor;
    }
}