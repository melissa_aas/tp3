package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.ArrayList;


@Entity
public class Groupe{
    private @Id @GeneratedValue long id;
    private String title;


    public Groupe(long id, String title){
        this.id = id;
        this.title = title;
    }

    public Groupe(String title){
        this.title = title;

    }

    public Groupe() {}

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

}

